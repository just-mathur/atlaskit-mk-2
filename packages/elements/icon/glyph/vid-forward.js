'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _index = require('../es5/index');

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var VidForwardIcon = function VidForwardIcon(props) {
  return _react2.default.createElement(_index2.default, _extends({ dangerouslySetGlyph: '<svg width="24" height="24" viewBox="0 0 24 24" focusable="false" role="presentation"><g fill="currentColor" fill-rule="evenodd"><path d="M3.768 4.189C3.344 3.833 3 3.996 3 4.55v14.888c0 .556.348.736.785.395l8.88-6.914c.433-.338.437-.903.016-1.256L3.768 4.19z"/><path d="M11.768 4.189c-.424-.356-.768-.193-.768.362v14.888c0 .556.348.736.785.395l8.88-6.914c.433-.338.437-.903.016-1.256L11.768 4.19z"/></g></svg>' }, props));
};
exports.default = VidForwardIcon;