// @flow
import Icon, { size } from './components/Icon';

export default Icon;
export { size };
