// @flow

import React from 'react';
import { CountrySelect } from './common/components';

const CountryExample = () => <CountrySelect placeholder="Choose a Country" />;

export default CountryExample;
