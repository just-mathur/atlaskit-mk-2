/* tslint:disable:variable-name */
import styled from 'styled-components';

export const AvatarPickerViewWrapper = styled.div`
  text-align: center;
`;

export const ModalHeader = styled.div`
  margin: 15px;
  font-weight: 500;
`;

export const CroppingWrapper = styled.div`
  display: inline-block;
`;

export const ModalFooterButtons = styled.div`
  text-align: right;
  width: 100%;
`;
