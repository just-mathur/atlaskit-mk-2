# @atlaskit/media-editor

## 4.0.2

## 4.0.1
- [patch] Fix eidtorCore binary [add6f5f](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/add6f5f)

## 4.0.0

## 3.4.0
- [minor] Add React 16 support. [12ea6e4](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/12ea6e4)

## 3.3.8

## 3.3.7

## 3.3.6
- [patch] Copy binaries into dist folder when building media-editor [a82d5da](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/a82d5da)

## 3.3.5

## 3.3.4

## 3.3.3

## 3.3.2

- [patch] bump icon dependency [da14956](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/da14956)
- [patch] bump icon dependency [da14956](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/da14956)

## 3.3.1
