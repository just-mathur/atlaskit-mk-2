export interface Hasher {
  hash(chunk: any): void;
}
