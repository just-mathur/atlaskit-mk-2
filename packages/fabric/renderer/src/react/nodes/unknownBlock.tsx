import * as React from 'react';
export default function UnknownBlock(props: React.Props<any>) {
  return <div>{props.children}</div>;
}
